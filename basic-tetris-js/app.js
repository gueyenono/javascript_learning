// Tutorial: https://www.youtube.com/watch?v=rAUn1Lom6dw

document.addEventListener('DOMContentLoaded', () => { // document refers to the HTML document

  const grid = document.querySelector('.grid')
  let squares = Array.from(document.querySelectorAll('.grid div'))
  const width = 10
  const scoreDisplay = document.querySelector('#score')
  const startBtn = document.querySelector('#start-button')
  let nextRandom = 0
  let timerId
  let score = 0

  const colors = [
    'orange',
    'red',
    'purple',
    'green',
    'blue'
  ]

  console.log(squares)

  //The Tetrominoes
const lTetromino = [
  [1, width+1, width*2+1, 2],
  [width, width+1, width+2, width*2+2],
  [1, width+1, width*2+1, width*2],
  [width, width*2, width*2+1, width*2+2]
]

const zTetromino = [
  [0,width,width+1,width*2+1],
  [width+1, width+2,width*2,width*2+1],
  [0,width,width+1,width*2+1],
  [width+1, width+2,width*2,width*2+1]
]

const tTetromino = [
  [1,width,width+1,width+2],
  [1,width+1,width+2,width*2+1],
  [width,width+1,width+2,width*2+1],
  [1,width,width+1,width*2+1]
]

const oTetromino = [
  [0,1,width,width+1],
  [0,1,width,width+1],
  [0,1,width,width+1],
  [0,1,width,width+1]
]

const iTetromino = [
  [1,width+1,width*2+1,width*3+1],
  [width,width+1,width+2,width+3],
  [1,width+1,width*2+1,width*3+1],
  [width,width+1,width+2,width+3]
]

const tetrominoes = [lTetromino, zTetromino, tTetromino, oTetromino, iTetromino]

  const tetrominoes2 = tetrominoes.map(tetromino => {
    return tetromino.map(rotation => {
      return rotation.map(index => {
        return index - 1
      })
    })
  })

  let currentPosition = 4
  let currentRotation = 0

  // Randomly select a Tetromino and its first rotation
  let random = Math.floor(Math.random() * tetrominoes2.length)
  let current = tetrominoes2[random][currentRotation]

// Draw the first rotation of the randomly chosen Tetromino
function draw() {
  current.forEach(index => {
    squares[currentPosition + index].classList.add('tetromino')
    squares[currentPosition + index].style.backgroundColor = colors[random]
  })
}

// Undraw the tetromino
function undraw() {
  current.forEach(index => {
    squares[currentPosition + index].classList.remove('tetromino')
    squares[currentPosition + index].style.backgroundColor = ''
  })
}

// Freeze function

function freeze() {
  if(current.some(index => squares[currentPosition + index + width].classList.contains('taken'))) {
    current.forEach(index => squares[currentPosition + index].classList.add('taken'))
    // Start a new Tetromino falling
    random = nextRandom
    nextRandom = Math.floor(Math.random() * tetrominoes2.length)
    current = tetrominoes2[random][currentRotation]
    currentPosition = 4
    draw()
    displayShape()
    addScore()
    gameOver()
  }
}

function moveDown() {
  undraw()
  currentPosition += width
  draw()
  freeze()
}


// Move the tetromino left, unless it as the edge of the grid
function moveLeft(){
  undraw()
  const isAtLeftEdge = current.some(index => (currentPosition + index) % width === 0)

  if(!isAtLeftEdge) currentPosition -= 1

  if(current.some(index => squares[currentPosition + index].classList.contains('taken'))){
    currentPosition += 1
  }

  draw()
}

// Move the tetromino right, unless it as the edge of the grid
function moveRight(){
  undraw()
  const isAtRightEdge = current.some(index => (currentPosition + index) % width === width - 1)

  if(!isAtRightEdge) currentPosition += 1

  if(current.some(index => squares[currentPosition + index].classList.contains('taken'))){
    currentPosition -= 1
  }

  draw()
}

// Rotate the tetrominoes
function rotate() {
  undraw()
  currentRotation ++
  if(currentRotation == current.length){ // if currentRotation gets to 4, make it go back to 0
    currentRotation = 0
  }
  current = tetrominoes2[random][currentRotation]
  draw()
}

// Show up-next tetromino in mini-grid
const displaySquares = document.querySelectorAll('.mini-grid div')
const displayWidth = 4
let displayIndex = 0
// let nextRandom = 0

// The terominoes without rotation
const upNextTetrominoes = [
  [1, displayWidth+1, displayWidth*2+1, 2], // l
  [0,displayWidth,displayWidth+1,displayWidth*2+1], //z
  [1,displayWidth,displayWidth+1,displayWidth+2], //t
  [0,1,displayWidth,displayWidth+1], // o
  [1,displayWidth+1,displayWidth*2+1,displayWidth*3+1] //i
]

// Display the shape in the mini-grid
function displayShape() {

  displaySquares.forEach(square => {
    square.classList.remove('tetromino')
    square.style.backgroundColor = ''
  })

  upNextTetrominoes[nextRandom].forEach(index => {
    displaySquares[displayIndex + index].classList.add('tetromino')
    displaySquares[displayIndex + index].style.backgroundColor = colors[nextRandom]
  })

}

//  Assign functions to keycodes
function control(e) {
  if(e.keyCode === 37) { // Press left
    moveLeft()
  } else if(e.keyCode === 38) { // Press up
    rotate()
  } else if(e.keyCode === 39) { // Press right
    moveRight()
  } else if(e.keyCode === 40) { // Press down
    moveDown()
  }
  console.log(`Button pressed: ${e.keyCode}`)
}

document.addEventListener('keyup', control)

// Add functionality to the Start/Pause Button
startBtn.addEventListener('click', () => {
  if(timerId) {
    clearInterval(timerId)
    timerId = null
  } else {

    // nextRandom = Math.floor(Math.random() * tetrominoes2.length)
    displayShape()
  }
})

// Make the tetromino move down every second
// timerId = setInterval(moveDown, 1000)

// Add score functions
function addScore() {
  for(let i = 0; i < 199; i+= width) {
    const row = [i, i+1, i+2, i+3, i+4, i+5, i+6, i+7, i+8, i+9]

    if(row.every(index => squares[index].classList.contains('taken'))) {
      score += 10
      scoreDisplay.innerHTML = score
      row.forEach(index => {
        squares[index].classList.remove('taken')
        squares[index].classList.remove('tetromino')
          squares[index].style.backgroundColor = ''
      })
      const squaresRemoved = squares.splice(i, width)
      squares = squaresRemoved.concat(squares)
      squares.forEach(cell => grid.appendChild(cell))
    }
  }
}

// GAME OVER

function gameOver(){
  if(current.some(index => squares[currentPosition + index].classList.contains('taken'))) {
    scoreDisplay.innerHTML = 'end'
    clearInterval(timerId)
  }
}

})
