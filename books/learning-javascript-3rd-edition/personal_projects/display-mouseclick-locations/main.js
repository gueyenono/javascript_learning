$(document).ready(function(){

  paper.install(window);
  paper.setup(document.getElementById("canvas"));

  tool = new Tool();
  tool.onMouseDown = function(event){
    console.log("x-coordinate: ", event.point.x, " and y-coordinate: ", event.point.y);
    // console.log("Button clicked");
  };

  paper.view.draw();
})
