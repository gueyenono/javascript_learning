/*
There are 6 primitive types

Number
String
Boolean
Null
Undefined
Symbol
*/

/*
The built-in object types

Array
Date
RegExp
Map and WeakMap
Set and WeakSet
*/

/*
The primitives: Number, String and Boolean have a corresponding object type.
*/

let count = 10;
const blue = 0x0000ff;
const umask = 0o0022
const roomTemp = 21.5;
const c = 3.0e6;
const e = -1.6e-19;
const inf = Infinity;
const ninf = -Infinity;
const nan = NaN;

// Useful properties of the Number object

const small = Number.EPSILON;
const max = Number.MAX_VALUE;
const minInt = Number.MIN_SAFE_INTEGER;
const bigInt = Number.MAX_SAFE_INTEGER;
const nInf = Number.NEGATIVE_INFINITY;
const Nan = Number.NaN;
const Inf = Number.POSITIVE_INFINITY;

let lst = [small, max, minInt, bigInt, nInf, Nan, Inf];

for(s of lst){
  console.log(`${s}`);
}

console.log(`\u2310P`);

// Multiline strings

const multiline = "line1\n\
line2";

console.log(multiline);
