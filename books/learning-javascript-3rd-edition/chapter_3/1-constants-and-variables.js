// Initialize a variable

let currentTempC = 22; // degrees celsius
console.log(currentTempC);

currentTempC = 22.5;
console.log(currentTempC);

// Initialize a variable with no value

let targetTempC; // equivalent to let targetTempC = undefined;
console.log(targetTempC);

// Initialize multiple variables simultaneously
let targetTempC2, room1 = "conference_room_a", room2 = "lobby";
let myList = [targetTempC2, room1, room2];

for(s of myList){
  console.log(`${s}`);
}

// The value of a constant cannot be changed after initialization
const ROOM_TEMP_C = 21.5, MAX_TEMP_C = 30;

console.log(`The current room temperature is ${ROOM_TEMP_C} degrees, but can go up to ${MAX_TEMP_C}.`)
