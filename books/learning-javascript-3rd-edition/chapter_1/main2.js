$(document).ready(function() {
  'use strict';

  paper.install(window);
  paper.setup(document.getElementById('mainCanvas'));

  var d;
  for (var x = 25; x < 400; x += 50) {
    for (var y = 25; y < 400; y += 50) {
      d = Shape.Circle(x, y, 25);
      d.fillColor = 'green';
      d.strokeColor = 'red';
    }
  }

  paper.view.draw();

})
