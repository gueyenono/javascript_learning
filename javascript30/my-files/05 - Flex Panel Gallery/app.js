document.addEventListener("DOMContentLoaded", () => {

  const panels = document.querySelectorAll(".panel")

  // Functions
  function toggleOpen() {
    this.classList.toggle("open")
  }

  function toggleActive() {
    this.classList.toggle("open-active")
  }

  // Events
  panels.forEach(panel => {
    panel.addEventListener("click", toggleOpen)
  })

  panels.forEach(panel => {
    panel.addEventListener("click", toggleActive)
  })

})
