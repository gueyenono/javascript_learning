document.addEventListener("DOMContentLoaded", () => {

  // Variables
  const endpoint = 'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json';
  const cityInput = document.querySelector("input.search")
  const suggestionList = document.querySelector("ul.suggestions")
  let cities = []

  // GET endpoint
  fetch(endpoint)
        .then(promise => promise.json())
        .then(data => cities.push(...data))

  // Functions
  function findMatches(wordsToMatch, cities) {
    return cities.filter(place => {
      // Here we need to figure out if the city or state matches what was searches
      const regex = new RegExp(wordsToMatch, "gi") //g: global, i: case-insensitive
      return place.city.match(regex) || place.state.match(regex)
    })
  }

  function displayMatches() {
    const matchArray = findMatches(this.value, cities)
    const html = matchArray.map(place => {
      const regex = new RegExp(this.value, "gi")
      const cityName = place.city.replace(regex, `<span class="hl">${this.value}</span>`)
      const stateName = place.state.replace(regex, `<span class="hl">${this.value}</span>`)

      return `
        <li>
          <span class="name">${cityName}, ${stateName}</span>
          <span class="population">${place.population}</span>
        </li>
      `
    }).join("")
    suggestionList.innerHTML = html
  }

  // Events
  cityInput.addEventListener("keyup", displayMatches)

})
