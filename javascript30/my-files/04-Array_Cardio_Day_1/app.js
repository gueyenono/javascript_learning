const inventors = [
  { first: 'Albert', last: 'Einstein', year: 1879, passed: 1955 },
  { first: 'Isaac', last: 'Newton', year: 1643, passed: 1727 },
  { first: 'Galileo', last: 'Galilei', year: 1564, passed: 1642 },
  { first: 'Marie', last: 'Curie', year: 1867, passed: 1934 },
  { first: 'Johannes', last: 'Kepler', year: 1571, passed: 1630 },
  { first: 'Nicolaus', last: 'Copernicus', year: 1473, passed: 1543 },
  { first: 'Max', last: 'Planck', year: 1858, passed: 1947 },
  { first: 'Katherine', last: 'Blodgett', year: 1898, passed: 1979 },
  { first: 'Ada', last: 'Lovelace', year: 1815, passed: 1852 },
  { first: 'Sarah E.', last: 'Goode', year: 1855, passed: 1905 },
  { first: 'Lise', last: 'Meitner', year: 1878, passed: 1968 },
  { first: 'Hanna', last: 'Hammarström', year: 1829, passed: 1909 }
];

const people = ['Beck, Glenn', 'Becker, Carl', 'Beckett, Samuel', 'Beddoes, Mick',
'Beecher, Henry', 'Beethoven, Ludwig', 'Begin, Menachem', 'Belloc, Hilaire',
'Bellow, Saul', 'Benchley, Robert', 'Benenson, Peter', 'Ben-Gurion, David', 'Benjamin',
'Walter', 'Benn, Tony', 'Bennington, Chester', 'Benson, Leana', 'Bent, Silas',
'Bentsen, Lloyd', 'Berger, Ric', 'Bergman, Ingmar', 'Berio, Luciano', 'Berle, Milton',
 'Berlin, Irving', 'Berne, Eric', 'Bernhard, Sandra', 'Berra, Yogi', 'Berry, Halle',
'Berry, Wendell', 'Bethea, Erin', 'Bevan, Aneurin', 'Bevel, Ken', 'Biden, Joseph',
'Bierce, Ambrose', 'Biko, Steve', 'Billings, Josh', 'Biondo, Frank', 'Birrell',
'Augustine', 'Black, Elk', 'Blair, Robert', 'Blair, Tony', 'Blake, William'];

// Array.prototype.filter()
// 1. Filter the list of inventors for those who were born in the 1500's

// inventors.forEach(inv => console.log(inv.year))

const inventors1500s = inventors.filter(inv => inv.year >= 1500 && inv.year <= 1599)
console.log(inventors1500s)

// Array.prototype.map()
// 2. Give us an array of the inventors first and last names

const inventorsNames = inventors.map(inv => [{first: inv.first, last: inv.last}])
console.log(inventorsNames)

// Array.prototype.sort()
// 3. Sort the inventors by birthdate, oldest to youngest

let ordered = inventors.sort((inv1, inv2) => inv1.year > inv2.year ? 1 : -1) // Ternary operators (similar to ifelse statements in R)
ordered = inventors.sort((inv1, inv2) => inv1.year - inv2.year) // Different way of sorting array
console.log(ordered)

// Array.prototype.reduce()
// 4. How many years did all the inventors live all together?

// My solution 1
const yearsLived = inventors.map(inv => inv.passed - inv.year)
const totalYears = yearsLived.reduce((a, b) => a + b, 0)
console.log(yearsLived)
console.log(totalYears)

// Another solution (more compact)
const totalYears2 = inventors.reduce((acc, inv) => acc + (inv.passed - inv.year), 0)
console.log(totalYears2)

// 5. Sort the inventors by years lived

inventors.forEach(inv => inv.time = inv.passed - inv.year)
console.table(inventors)

const sortedByTime = inventors.sort((a, b) => (a.passed - a.year) - (b.passed - b.year))
console.table(sortedByTime)

// 6. create a list of Boulevards in Paris that contain 'de' anywhere in the name
// https://en.wikipedia.org/wiki/Category:Boulevards_in_Paris

// Run this in the devtools on the page
// const names = document.querySelectorAll(".mw-content-ltr a")
// const blvdNames = Array.from(names).map(x => x.textContent)
// blvdNames.filter(x => x.includes(" de "))

// 7. sort Exercise
// Sort the people alphabetically by last name

const sortedByLast = people.sort((a, b) => {
  const [aLast, aFirst] = a.split(", ")
  const [bLast, bFirst] = b.split(", ")
  return aLast - bLast
})

console.log(sortedByLast)

// 8. Reduce Exercise
// Sum up the instances of each of these
const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk', 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck' ];

const transportation = data.reduce((obj, item) => {
  if(!obj[item]){
    obj[item] = 0
  }
  obj[item]++
  return obj
}, {}) // Start with a blank object

console.log(transportation)
