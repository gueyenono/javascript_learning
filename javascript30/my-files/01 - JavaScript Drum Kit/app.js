document.addEventListener("DOMContentLoaded", () => {

  // Function for playig sound when key is pressed
  function playSound(event) {
    // Declare variables
    const audio = document.querySelector(`audio[data-key="${event.keyCode}"]`)
    const key = document.querySelector(`.key[data-key="${event.keyCode}"]`)

    if(!audio || !key) return; // Stop the function

    audio.currentTime = 0 // Rewind to start
    audio.play()

    key.classList.add("playing")
  }

  // Function for removing transition
  function removeTransition(event){
    if(event.propertyName !== "transform") return; // Stop if no transition
    // console.log(this)
    this.classList.remove("playing")
  }

  const keys = document.querySelectorAll(".key")
  keys.forEach(key => {
    key.addEventListener("transitionend", removeTransition)
  })

  // Play audio on the press of button
  window.addEventListener("keydown", playSound)

})
