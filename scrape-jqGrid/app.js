document.addEventListener("DOMContentLoaded", () => {
  //<![CDATA[
  $(function () {
      "use strict";
      $("#grid").jqGrid({
          colModel: [
              { name: "firstName" },
              { name: "lastName" }
          ],
          data: [
              { id: 10, firstName: "Angela", lastName: "Merkel" },
              { id: 20, firstName: "Vladimir", lastName: "Putin" },
              { id: 30, firstName: "David", lastName: "Cameron" },
              { id: 40, firstName: "Barack", lastName: "Obama" },
              { id: 50, firstName: "François", lastName: "Hollande" }
          ]
      });
  });
  //]]>

  var fullData = jQuery("#grid").jqGrid('getRowData');

  console.log(fullData)

  let jq1 = document.createElement('script')
  let jq2 = document.createElement('script')

  jq1.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"
  jq2.src = "https://cdnjs.cloudflare.com/ajax/libs/free-jqgrid/4.15.5/jquery.jqgrid.min.js"

  document.getElementsByTagName('head')[0].appendChild(jq1);
  document.getElementsByTagName('head')[0].appendChild(jq2);

  let mydata = jQuery("#contentjqxGrid").jqGrid('getRowData');


})
